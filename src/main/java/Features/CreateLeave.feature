Feature: Leave Creation
  Scenario: Create a leave.
    Given User is present on login Page.
    Then  User provide username and password and click on submit button.
    Then  User is navigated to Leave page
    Then  User is on Add new page
    Then  Leave details are added
    Then  Leaves details Verified and Submitted
    Then  User is Logged out
    Then  Browser is closed

