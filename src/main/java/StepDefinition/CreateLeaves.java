package StepDefinition;

import base.TestBase;
import commonUtils.PropertyReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;
import pageObjects.*;

import java.io.IOException;

public class CreateLeaves extends TestBase {
    WebDriver driver;
    LoginPage loginPage;
    LandingPage landingPage;
    LeavesPage leavesPage;
    AddLeavePage addLeavePage;
    LeavesDetails leavesDetails;

    @Given("^User is present on login Page\\.$")
    public void openBrowser() throws IOException {
        PropertyReader.ReadFile();
        driver = driverInitializer();
        loginPage = openApp();
    }
    @Then("^User provide username and password and click on submit button\\.$")
    public void loginToApp() {
        landingPage = loginPage.loginToApp();
    }

    @Then("^User is navigated to Leave page$")
    public void userIsNavigatedToLeavePage() {
        leavesPage =  landingPage.navigationBar(LandingPage.NavigationBar.Leaves);
    }
    @Then("^User is on Add new page$")
    public void userIsOnAddNewPage() {
        addLeavePage = leavesPage.clickAddNewBtn();
    }
    @Then("^Leave details are added$")
    public void leaveDetailsAreAdded(){
        leavesDetails = addLeavePage.createLeave();
    }
    @Then("^Leaves details Verified and Submitted$")
    public void leavesDetailsVerifiedAndSubmitted() {
        leavesDetails.verifyLeavesDetails().
                clickOnSubmitLeave().assertLeaveSubmit();
    }
    @Then("^User is Logged out$")
    public void userIsLoggedOut() {
        landingPage.logOut(LandingPage.NavigationBar.LogOutBtn);
    }
    @Then("^Browser is closed$")
    public void browserIsClosed() {
        closeApp();
    }

}
