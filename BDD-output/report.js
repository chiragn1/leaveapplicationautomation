$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/java/Features/CreateLeave.feature");
formatter.feature({
  "line": 1,
  "name": "Leave Creation",
  "description": "",
  "id": "leave-creation",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "Create a leave.",
  "description": "",
  "id": "leave-creation;create-a-leave.",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "User is present on login Page.",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "User provide username and password and click on submit button.",
  "keyword": "Then "
});
formatter.step({
  "line": 5,
  "name": "User is navigated to Leave page",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User is on Add new page",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "Leave details are added",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Leaves details Verified and Submitted",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User is Logged out",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Browser is closed",
  "keyword": "Then "
});
formatter.match({
  "location": "CreateLeaves.openBrowser()"
});
formatter.result({
  "duration": 13564054976,
  "status": "passed"
});
formatter.match({
  "location": "CreateLeaves.loginToApp()"
});
formatter.result({
  "duration": 1284682425,
  "status": "passed"
});
formatter.match({
  "location": "CreateLeaves.userIsNavigatedToLeavePage()"
});
formatter.result({
  "duration": 209271943,
  "error_message": "org.openqa.selenium.NoSuchElementException: Cannot locate an element using xpath\u003d//a[@href\u003d\"/aways\"]\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027Chirags-MacBook-Pro.local\u0027, ip: \u0027fe80:0:0:0:48c:9d8c:996d:133b%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.15.6\u0027, java.version: \u002714.0.2\u0027\nDriver info: driver.version: RemoteWebDriver\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:327)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:428)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat pageObjects.LandingPage.navigationBar(LandingPage.java:72)\n\tat StepDefinition.CreateLeaves.userIsNavigatedToLeavePage(CreateLeaves.java:33)\n\tat ✽.Then User is navigated to Leave page(src/main/java/Features/CreateLeave.feature:5)\n",
  "status": "failed"
});
formatter.match({
  "location": "CreateLeaves.userIsOnAddNewPage()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateLeaves.leaveDetailsAreAdded()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateLeaves.leavesDetailsVerifiedAndSubmitted()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateLeaves.userIsLoggedOut()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateLeaves.browserIsClosed()"
});
formatter.result({
  "status": "skipped"
});
});